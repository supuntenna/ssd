<?php


if(isset($_POST['username'],$_POST['password'])){
	$uname = $_POST['username'];
	$pwd = $_POST['password'];
	if($uname == 'admin123' && $pwd == 'admin123'){
		echo 'You have Successfully Logged in';
		session_start();
		$_SESSION['token'] = base64_encode(openssl_random_pseudo_bytes(32));
		$session_id = session_id();
		setcookie('sessionCookie',$session_id,time()+60*60*24*365,'/');
		setcookie('csrfCookie',$_SESSION['token'],time()+60*60*24*365,'/');
		
	}
	else{
		echo 'Incorrect username or password';
		exit();
	}	
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Cross Site Request Forgery</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet"  href="./public/styles/style.css">
		<link rel="stylesheet"  href="./public/styles/bootstrap.min.css">
		<script>
	
			$(document).ready(function(){
	
				var name = "csrfCookie" + "=";
				var csrf_cookie_value = "";
				var decoded_csrf_cookie = decodeURIComponent(document.cookie);
				var dcs = decoded_csrf_cookie.split(';');
			
				for(var i = 0; i <dcs.length; i++) {
					var dc = dcs[i];
					while (dc.charAt(0) == ' ') {
						dc = dc.substring(1);
					}
			
					if (dc.indexOf(name) == 0) {
						cookie_value = dc.substring(name.length, dc.length);
						document.getElementById("csrf_token_to_be_generated").setAttribute('value', cookie_value) ;
					}
				}	
			});		
		</script>	
	</head>
	<body style="background-image: linear-gradient(to bottom right, blue, red)">
		<div class="container">
			<div class="row">
			  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
				<div class="card card-signin my-5">
				  <div class="card-body">
					<h5 class="card-title text-center">Update Your Status</h5>
					<form name="updateStatus" class="form-signin" action="results.php" method="post">
					  <textarea rows="10" placeholder="Write here your status" name="updatepost"></textarea>
						<input type="submit" value="Update" class="btn btn-primary btn-block btn-large">
						<div id="csrfTokenDiv">
							<input type="hidden" name="token" value="" id="csrf_token_to_be_generated"/>
						</div>

					    <div class="custom-control custom-checkbox mb-3">
						<input type="checkbox" class="custom-control-input" id="customCheck1">
						</div>
					  <hr class="my-4">
					 </form>
				  </div>
				</div>
			  </div>
			</div>
		</div>					
	</body> 
</html>
