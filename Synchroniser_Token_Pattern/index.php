<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Cross Site Request Forgery Attack(DSCP)</title>
		<link rel="stylesheet"  href="./public/styles/style.css">
		<link rel="stylesheet"  href="./public/styles/bootstrap.min.css">
	</head>
	<body style="background-image: linear-gradient(to bottom right, blue, red)">
	  <div class="container">
		<div class="row">
		  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
			<div class="card card-signin my-5">
			  <div class="card-body">
				<h5 class="card-title text-center">Sign In</h5>
				<form name="login" action="updateStatus.php" method="post" class="form-signin">
				  <div class="form-label-group">
					<input type="text" name="username" placeholder="Username" required="required" class="form-control"/>
					<label for="inputEmail">UserName</label>
				  </div>

				  <div class="form-label-group">
					<input type="password" name="password" placeholder="Password" required="required" class="form-control"/>
					<label for="inputPassword">Password</label>
				  </div>
				  <div class="custom-control custom-checkbox mb-3">
					<input type="checkbox" class="custom-control-input" id="customCheck1">
					<label class="custom-control-label" for="customCheck1">Remember password</label>
				  </div>
				  <button type="submit" class="btn btn-primary btn-block btn-large">Log me in</button>
				  <hr class="my-4">
				 </form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</body>
</html>



